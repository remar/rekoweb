# rekoweb

## Bakgrund

[REKO-ringar](https://hushallningssallskapet.se/forskning-utveckling/reko/)
är sammanslutningar där konsumenter kommer i direkt kontakt med lokala
matproducenter.

De flesta REKO-ringar använder sig av Facebook för administration, där
producenterna skriver inlägg i en grupp och konsumenter skriver
kommentarer på inläggen för att lägga beställningar.

Det här sättet att administrera känns onödigt krångligt och
omständligt. Istället syftar detta projekt till att helt ersätta
Facebook för administration av REKO-ringar. Förhoppningen är att det
ska gå betydligt smidigare både att beställa varor som konsument och
även hantera beställningarna som producent.

## Intressenter

Fyra olika grupper av användare har identifierats: konsumenter,
producenter, ring-administratörer och web-administratörer.

### Konsumenter

* Vill veta när nästa utlämning av varor sker, och på vilken plats
* Vill veta vad för produkter som erbjuds
* Vill kunna beställa varor
* Vill ha en sammanfattning av vad som ska betalas till vem (swish osv.)

### Producenter

* Vill kunna vara med på en utlämning
* Vill kunna erbjuda varor på en utlämning
* Vill kunna ange hur mycket varor som finns att beställa
* Vill kunna administrera utlämningen på smidigt sätt
* Vill kunna verifiera att betalning har skett

### Ring-administratörer

* Vill kunna lägga till producent
* Vill kunna ansluta en användare som ansvarig/admin för producent

### Web-administratörer

* Vill kunna lägga till ny reko-ring
* Vill kunna ansluta användare som ring-administratör för reko-ring

## Alternativ

Jag har hittat två alternativ till Facebook för reko-ringar: FarmUP
och Local Food Nodes.

[FarmUP](https://www.farmup.se/) ser ut att bara vara en app till
Android-telefoner vilket rejält begränsar dess nytta.

[Local Food Nodes](https://localfoodnodes.org/) ser på ytan ut att
vara närmare det som jag vill utveckla. Den kommersiella modellen är
dock undermålig eftersom den kräver att den som vill beställa från en
producent behöver betala pengar till Local Food Nodes.
